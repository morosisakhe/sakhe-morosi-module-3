import 'package:flutter/material.dart';
import 'package:sakhe_morosi_module_3/login.dart';

void main() {
  runApp (MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => const MaterialApp(
    home: Login(),
  );
}
 
